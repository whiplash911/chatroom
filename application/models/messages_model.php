<?php

class messages_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function setMessage($message)
    {
        $this->db->insert('messages', $message);
    }
    
    public function getMessages($conditions = array())
    {
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }
        
        $this->db->select('*');
        $this->db->from('messages');
        $this->db->limit('20');
        $this->db->order_by('id', 'asc');
        
        $result = $this->db->get();
        
        return $result;
    }
}
