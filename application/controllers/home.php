<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('messages_model');
    }

    public function index()
    {
        $data['title'] = 'Welcome to the Chatroom';
        $this->load->view('chatroom_view', $data);
    }
    
    public function postMessage()
    {
        if ($this->input->post('message')) {

            $messageData = array(
                'message' => $this->input->post('message')
            );
            $this->messages_model->setMessage($messageData);
            
            $response = array(
                'result' => 'success'
            );
        } else {
            $response = array(
                'result' => 'error'
            );
        }
        
        echo json_encode($response);
        die;
    }
    
    public function getLastMessage()
    {
        $messages = $this->messages_model->getMessages()->result();
        
        foreach($messages as $key => $message) {
            echo '<p>'.$message->message.'</p>';
        }
    }
}
