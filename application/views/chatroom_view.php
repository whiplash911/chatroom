<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?= $title; ?></title>
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.3.min.js"></script>
    <style>
        body {
            background: url('http://images.forwallpaper.com/files/thumbs/preview/24/249933__paper-stains-spray-stained-paper-alexander-gg-deviantart_p.jpg') no-repeat center center fixed;
            margin-left: 50px; 
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            background-repeat: x;
        }
        
        #main_header {
            color: #0F7EA6;
            font-family: helvetica;
        }
        
        #message {
            margin-top: 30px;
            width: 200px;
            height: 20px;
            border: 2px solid #0086b3;
        }
        
        #submit {
            width: 80px;
            height: 27px;
            border: 2px solid #0086b3;
            font-family: verdana;
            font-size: 14px;
            color: #0086b3;
            background-color: #e3e3e3
        }
        
        .content {
            background-image: url('http://spyrestudios.com/wp-content/uploads/christmas-paper/crumpled-paper.jpg');
            background-size: cover;
            overflow: auto;
            height: 250px;
            width: 300px;
            border: #333 1px solid;
            border-radius: 6px;
            -moz-border-radius: 6px;
        }
        
        .content p{
            color: #0F7EA6;
            padding-left: 10px;       
            font-family: arial;
            font-size: 18px;
        }
    </style>
</head>
<body>
    <h1 id='main_header'>Welcome</h1>

    <div class="content"></div>
    <div class="errors"></div>
        
    <form id="userInput" method="post">
        <label for="message"></label>
        <input id="message" name="message" type="text"/>
        <input id="submit" type="submit" name="submit" value="Send"/>
    </form>
</body>

<script> 
function loopLi() {
    setInterval(function() {
        // this code is executed every 3 seconds     
        $.ajax({
            url: 'index.php/home/getLastMessage',
            type: 'get',
            success: function(messages){
                $('.content').html(messages);
            }
        });
    }, 3000);
}

$(function(){
    
    loopLi();
    
    $('#userInput').submit(function(e){
        e.preventDefault();
        var $message = $('#message').val();
        
        if ($message.length > 0) {
            $.ajax({
            url: 'index.php/home/postMessage',
            type: 'post',
            dataType: 'json',
            data: {
                message: $message
            },
            success: function(){
                $('#message').val('');
            }
        });
        } else {
            alert('Please type a message!');
        }
    })
    
});
</script>
</html>